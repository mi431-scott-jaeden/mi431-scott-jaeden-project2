using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using JetBrains.Annotations;

public class MazeGenerator : MonoBehaviour
{
    [SerializeField]
    private MazeCell _mazeCellPrefab;

    [SerializeField]
    private List<GameObject> mazeBonus;

    public float bonusChance = 25f;
    public float multRoomChance = 25f;
    public float additionalRoomChance = 25f;
    public int maxMultSize = 4;

    public GameObject start;
    public GameObject end;

    [SerializeField]
    private int _mazeWidth;

    [SerializeField]
    private int _mazeDepth;

    private MazeCell[,] _mazeGrid;
    private bool madeStart = false;
    private bool madeEnd = false;
    public int spacesToEnd = 20;

    void Start()
    {
        int currentSpaces = 0;
        _mazeGrid = new MazeCell[_mazeWidth, _mazeDepth];
        for (int x = 0; x < _mazeWidth; x++)
        {
            for (int z = 0; z < _mazeDepth; z++)
            {
                if (!madeStart)
                {
                    Instantiate(start, new Vector3(x, 0, z), Quaternion.identity);
                    madeStart = true;
                }
                else if ((!madeEnd && currentSpaces >= spacesToEnd && Random.Range(0f, 100f) < 1f) || (x == _mazeWidth && z == _mazeDepth))
                {
                    Instantiate(end, new Vector3(x, 0, z), Quaternion.identity);
                    madeEnd = true;
                }

                if (_mazeGrid[x, z] == null)
                {
                    _mazeGrid[x, z] = Instantiate(_mazeCellPrefab, new Vector3(x, 0, z), Quaternion.identity);
                    currentSpaces++;
                }
                if (Random.Range(0f, 100f) < multRoomChance)
                {
                    int currentSize = 1;
                    bool run = true;
                    //Make a random color to make multi-rooms more distinct 
                    Color col = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 255f);
                    //Make a list of list of ints to be able to pull multi-room tiles at random
                    List<List<int>> incTile = new List<List<int>> { new List<int> { x, z } };
                    do
                    {
                        List<int> chosenTile = incTile[Random.Range(0, incTile.Count())];
                        int direction = Random.Range(-2, 2);
                                               
                        if (direction == 0 && (chosenTile[0] < _mazeWidth - 1))
                        {
                            if (_mazeGrid[chosenTile[0] + 1, z] == null)
                            {
                                _mazeGrid[chosenTile[0] + 1, z] = Instantiate(_mazeCellPrefab, new Vector3(chosenTile[0] + 1, 0, z), Quaternion.identity);
                                _mazeGrid[chosenTile[0] + 1, z].ClearLeftWall();
                                _mazeGrid[chosenTile[0], z].ClearRightWall();
                                _mazeGrid[chosenTile[0] + 1, z].changeWallColor(col);
                                _mazeGrid[chosenTile[0], z].changeWallColor(col);
                                incTile.Add(new List<int> { chosenTile[0] + 1, z });
                                currentSize += 1;
                                currentSpaces++;
                            }
                        }
                        else if (direction == 1 && (chosenTile[1] < _mazeDepth - 1))
                        {
                            if (_mazeGrid[x, chosenTile[1] + 1] == null)
                            {
                                _mazeGrid[x, chosenTile[1] + 1] = Instantiate(_mazeCellPrefab, new Vector3(x, 0, chosenTile[1] + 1), Quaternion.identity);
                                _mazeGrid[x, chosenTile[1] + 1].ClearBackWall();
                                _mazeGrid[x, chosenTile[1]].ClearFrontWall();
                                _mazeGrid[x, chosenTile[1] + 1].changeWallColor(col);
                                _mazeGrid[x, chosenTile[1]].changeWallColor(col);
                                incTile.Add(new List<int> { x, chosenTile[1] + 1 });
                                currentSize += 1;
                                currentSpaces++;
                            }
                        }
                        else if (direction == -1 && (chosenTile[0] > 1))
                        {
                            if (_mazeGrid[chosenTile[0] - 1, z] == null)
                            {
                                _mazeGrid[chosenTile[0] - 1, z] = Instantiate(_mazeCellPrefab, new Vector3(chosenTile[0] - 1, 0, z), Quaternion.identity);
                                _mazeGrid[chosenTile[0] - 1, z].ClearRightWall();
                                _mazeGrid[chosenTile[0], z].ClearLeftWall();
                                _mazeGrid[chosenTile[0] - 1, z].changeWallColor(col);
                                _mazeGrid[chosenTile[0], z].changeWallColor(col);
                                incTile.Add(new List<int> { chosenTile[0] - 1, z });
                                currentSize += 1;
                                currentSpaces++;
                            }
                        }
                        else if (direction == -2 && (chosenTile[1] > 1))
                        {
                            if (_mazeGrid[x, z - 1] == null)
                            {
                                _mazeGrid[x, chosenTile[1] + 1] = Instantiate(_mazeCellPrefab, new Vector3(x, 0, chosenTile[1] + 1), Quaternion.identity);
                                _mazeGrid[x, chosenTile[1] + 1].ClearFrontWall();
                                _mazeGrid[x, chosenTile[1]].ClearBackWall();
                                _mazeGrid[x, chosenTile[1] + 1].changeWallColor(col);
                                _mazeGrid[x, chosenTile[1]].changeWallColor(col);
                                incTile.Add(new List<int> { x, chosenTile[1] - 1 });
                                currentSize += 1;
                                currentSpaces++;
                            }
                        }
                        if (Random.Range(0, 100) < multRoomChance)
                        {
                            run = false;
                        }
                    } while (currentSize < maxMultSize && run == true);
                }
            }
        }
            GenerateMaze(null, _mazeGrid[0, 0]);
        
    }
    private void GenerateMaze(MazeCell previousCell, MazeCell currentCell)
    {
        currentCell.Visit();
        ClearWalls(previousCell, currentCell);
        MazeCell nextCell;

        do
        {
            nextCell = GetNextUnvisitedCell(currentCell);

            if (nextCell != null)
            {
                GenerateMaze(currentCell, nextCell);
            }
            else
            {
                float rolledValue = Random.Range(0f, 100f);
                if (rolledValue < bonusChance)
                {
                    Quaternion rotation = Quaternion.identity;
                    float numCells = 0f;
                    Vector3 bonusPos = checkDirectionMag(previousCell, currentCell, ref numCells, ref rotation);
                    
                    bonusPos.x *= .8f;
                    bonusPos.z *= .8f;
                    
                    if (numCells == 3f)
                    {
                        GameObject bonus = Instantiate(mazeBonus[Random.Range(0, mazeBonus.Count)], currentCell.transform);
                        bonus.transform.localPosition = bonusPos;
                        bonus.transform.rotation = rotation;
                    }
                }
            }
        } while (nextCell != null);
    }

    private MazeCell GetNextUnvisitedCell(MazeCell currentCell)
    {
        var unvisitedCells = GetUnvisitedCells(currentCell);
        return unvisitedCells.OrderBy(_ => Random.Range(1, 10)).FirstOrDefault();
    }

    private IEnumerable<MazeCell> GetUnvisitedCells(MazeCell currentCell) 
    {
        int x = (int)currentCell.transform.position.x;
        int z = (int)currentCell.transform.position.z;

        if (x + 1 < _mazeWidth)
        {
            var cellToRight = _mazeGrid[x + 1, z];

            if (cellToRight.IsVisited == false)
            {
                yield return cellToRight;
            }
        }
        if (x - 1 >= 0)
        {
            var cellToLeft = _mazeGrid[x - 1, z];

            if (cellToLeft.IsVisited == false)
            {
                yield return cellToLeft;
            }
        }
        if (z + 1 < _mazeDepth)
        {
            var cellToFront = _mazeGrid[x, z + 1];

            if (cellToFront.IsVisited == false)
            {
                yield return cellToFront;
            }
        }
        if (z - 1 >= 0)
        {
            var cellToBack = _mazeGrid[x, z - 1];

            if (cellToBack.IsVisited == false)
            {
                yield return cellToBack;
            }
        }
    }

    private void ClearWalls(MazeCell previousCell, MazeCell currentCell)
    {
        if (previousCell == null)
        {
            return;
        }
        if (previousCell.transform.position.x < currentCell.transform.position.x)
        {
            previousCell.ClearRightWall();
            currentCell.ClearLeftWall();
            return;
        }
        if (previousCell.transform.position.x > currentCell.transform.position.x)
        {
            previousCell.ClearLeftWall();
            currentCell.ClearRightWall();
            return;
        }
        if (previousCell.transform.position.z < currentCell.transform.position.z) 
        {
            previousCell.ClearFrontWall();
            currentCell.ClearBackWall();
            return;
        }
        if (previousCell.transform.position.z > currentCell.transform.position.z) 
        {
            previousCell.ClearBackWall();
            currentCell.ClearFrontWall();
            return;
        }
    }
    private Vector3 checkDirectionMag(MazeCell previousCell, MazeCell currentCell, ref float numWalls, ref Quaternion rotation)
    {
        numWalls = currentCell.checkWalls();
        if (previousCell.transform.position.x < currentCell.transform.position.x)
        {
            rotation = new Quaternion(0, 45, 0, 0);
            return new Vector3(1f, 0f, 0f);
        }
        else if (previousCell.transform.position.x > currentCell.transform.position.x)
        {
            rotation = new Quaternion(0, 45, 0, 0);
            return new Vector3(-1f, 0f, 0f);
        }
        else if (previousCell.transform.position.z < currentCell.transform.position.z)
        {
            rotation = new Quaternion(0, 0, 0, 0);
            return new Vector3(0f, 0f, 1f);
        }
        else if (previousCell.transform.position.z > currentCell.transform.position.z)
        {
            rotation = new Quaternion(0, 90, 0, 0);
            return new Vector3(0f, 0f, -1f);
        }
        else 
            return Vector3.zero;
    }
}
