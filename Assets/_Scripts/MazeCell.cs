using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeCell : MonoBehaviour
{
    [SerializeField]
    private GameObject _leftWall;

    [SerializeField]
    private GameObject _rightWall;

    [SerializeField]
    private GameObject _frontWall;

    [SerializeField]
    private GameObject _backWall;

    [SerializeField]
    private GameObject _unvisitedBlock;

    public bool IsVisited { get; private set; }

    public void Visit()
    {
        IsVisited = true;
        _unvisitedBlock.SetActive(false);
    }

    public void ClearLeftWall()
    {
        _leftWall.SetActive(false);
    }
    public void ClearRightWall() 
    {
        _rightWall.SetActive(false);
    }
    public void ClearFrontWall() 
    {
        _frontWall.SetActive(false);
    }
    public void ClearBackWall() 
    {
        _backWall.SetActive(false);
    }
    public float checkWalls()
    {
        float retVal = 0f;
        if(_leftWall.activeInHierarchy)
        {
            retVal += 1f;
        }
        if (_rightWall.activeInHierarchy)
        {
            retVal += 1f;
        }
        if (_frontWall.activeInHierarchy)
        {
            retVal += 1f;
        }
        if (_backWall.activeInHierarchy)
        {
            retVal += 1f;
        }
        return retVal;
    }
    public void changeWallColor(Color cust)
    {//A function to change the walls of a cell to a random color
        Renderer rend = _leftWall.GetComponentInChildren<Renderer>();
        rend.material.SetColor("_Color", cust);

        rend = _rightWall.GetComponentInChildren<Renderer>();
        rend.material.SetColor("_Color", cust);

        rend = _frontWall.GetComponentInChildren<Renderer>();
        rend.material.SetColor("_Color", cust);

        rend = _backWall.GetComponentInChildren<Renderer>();
        rend.material.SetColor("_Color", cust);
    }
}
